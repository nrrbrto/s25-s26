
//TASK: Intergrate the use of an NPM Project in Node JS. 

  //1. INITALIZE an NPM(Node Package Manager) into the local project. 

      //(2 ways to perform this task)

      //METHOD 1: npm init 

      //METHOD 2: npm init -y(yes)

      //package.json => 'The heart of any Node projects'. it records all the important metadata about the project, (libraries, packages, function) that makes up the system or application. 

      //2 mins (send a screenshot of your package.json) in HO

      //Now, that we have integrated an NPM into our project, lets solve some issues we are facing in our local project. 

        //issue: needing to restart the project whenever changes occur.

        //solve: using NPM lets install a dependency that will allow to automatically fix (hotfix) changes in our app. 

        //=> 'nodemon' package

        //BASIC SKILLS using NPM

        //-> install package
          //1st method: npm install <name of packages>
          //2nd method: npm i <name of packages>

          //note: you can also install multiple packages at the same time. 
             //nodemon
             //express
             //bcrypt

        //-> uninstall or remove a package
           //1st method: npm uninstall <name of package>
           //2nd method: npm un <name of package>

        //nodemon utility => is a CLI utility tool, it's task is to wrap your node js app and watch for any changes in the file system and automatically restart/hotfic the process. 

        //IF this is the first time you local machine would us a platform/technology, the machine would need to recognize the technology first

        //ISSUE: command not found

        //YOU INSTALL IT ON A 'GLOBAL' SCALE. 
        //'GLOBAL' => the file system structure of the whole machine.
        //npm install -g nodemon 

        //After installing the new package, register it to the package.json file.

        //nodemon -> auto hotfix for changes done in the project.
        //node -> plain run time environment
